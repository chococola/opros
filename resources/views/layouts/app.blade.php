<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons' rel="stylesheet">
    <link href="https://unpkg.com/vuetify/dist/vuetify.min.css" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, minimal-ui">


    <!-- Styles -->
</head>
<body>
    <div id="app">

        <main class="py-4">

            <form id="opros-form" action="/save" method="post">
                @csrf
                <input type="text" hidden id="firstname" name="firstname"/>
                <input type="text" hidden id="secondname" name="secondname"/>
                <input type="text" hidden id="class" name="class"/>
                <input type="number" hidden id="slider1-value" name="slider1-value"/>
                <input type="number" hidden id="slider2-value" name="slider2-value"/>
                <input type="number" hidden id="slider3-value" name="slider3-value"/>
                <input type="number" hidden id="slider4-value" name="slider4-value"/>
                <input type="number" hidden id="slider5-value" name="slider5-value"/>
                <input type="number" hidden id="slider6-value" name="slider6-value"/>
                <input type="number" hidden id="slider7-value" name="slider7-value"/>
                <input type="number" hidden id="slider8-value" name="slider8-value"/>
                <input type="number" hidden id="slider9-value" name="slider9-value"/>
                <input type="number" hidden id="slider10-value" name="slider10-value"/>
                <input type="number" hidden id="slider11-value" name="slider11-value"/>
                <input type="number" hidden id="slider12-value" name="slider12-value"/>
                <input type="number" hidden id="slider13-value" name="slider13-value"/>
                <input type="number" hidden id="slider14-value" name="slider14-value"/>
                <input type="number" hidden id="slider15-value" name="slider15-value"/>
            </form>
            @yield('content')
        </main>
    </div>
</body>
</html>
