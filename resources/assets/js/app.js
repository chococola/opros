
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
import Vue from 'vue';
import Vuetify from 'vuetify'
import VueGoodWizard from 'vue-good-wizard';

Vue.use(VueGoodWizard);
Vue.use(Vuetify)

Vue.component('wizard-component', require('./components/WizardComponent.vue'));
Vue.component('slider1-component', require('./components/Slider1Component.vue'));
Vue.component('slider2-component', require('./components/Slider2Component.vue'));
Vue.component('slider3-component', require('./components/Slider3Component.vue'));
Vue.component('slider4-component', require('./components/Slider4Component.vue'));
Vue.component('slider5-component', require('./components/Slider5Component.vue'));
Vue.component('slider6-component', require('./components/Slider6Component.vue'));
Vue.component('slider7-component', require('./components/Slider7Component.vue'));
Vue.component('slider8-component', require('./components/Slider8Component.vue'));
Vue.component('slider9-component', require('./components/Slider9Component.vue'));
Vue.component('slider10-component', require('./components/Slider10Component.vue'));
Vue.component('slider11-component', require('./components/Slider11Component.vue'));
Vue.component('slider12-component', require('./components/Slider12Component.vue'));
Vue.component('slider13-component', require('./components/Slider13Component.vue'));
Vue.component('slider14-component', require('./components/Slider14Component.vue'));
Vue.component('slider15-component', require('./components/Slider15Component.vue'));

const app = new Vue({
    el: '#app'
});
