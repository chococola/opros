<?php

namespace App\Http\Controllers;

use App\Opros\Models\Opros;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class HomeController extends Controller
{

    /**
     * Сохранение результатов опроса
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function save(Request $request)
    {
        $attributes = $request->only([
            'firstname',
            'secondname',
            'class',
            'slider1-value',
            'slider2-value',
            'slider3-value',
            'slider4-value',
            'slider5-value',
            'slider6-value',
            'slider7-value',
            'slider8-value',
            'slider9-value',
            'slider10-value',
            'slider11-value',
            'slider12-value',
            'slider13-value',
            'slider14-value',
            'slider15-value',

        ]);

        try{
            Opros::create([
                'user_first_name' => isset($attributes['firstname']) ? $attributes['firstname'] : null,
                'user_second_name' => isset($attributes['secondname']) ? $attributes['secondname'] : null,
                'user_class' => isset($attributes['secondname']) ? $attributes['secondname'] : null,
                'question_1' => isset($attributes['slider1-value']) ? $attributes['slider1-value'] : 0,
                'question_2' => isset($attributes['slider2-value']) ? $attributes['slider2-value'] : 0,
                'question_3' => isset($attributes['slider3-value']) ? $attributes['slider3-value'] : 0,
                'question_4' => isset($attributes['slider4-value']) ? $attributes['slider4-value'] : 0,
                'question_5' => isset($attributes['slider5-value']) ? $attributes['slider5-value'] : 0,
                'question_6' => isset($attributes['slider6-value']) ? $attributes['slider6-value'] : 0,
                'question_7' => isset($attributes['slider7-value']) ? $attributes['slider7-value'] : 0,
                'question_8' => isset($attributes['slider8-value']) ? $attributes['slider8-value'] : 0,
                'question_9' => isset($attributes['slider9-value']) ? $attributes['slider9-value'] : 0,
                'question_10' => isset($attributes['slider10-value']) ? $attributes['slider10-value'] : 0,
                'question_11' => isset($attributes['slider11-value']) ? $attributes['slider11-value'] : 0,
                'question_12' => isset($attributes['slider12-value']) ? $attributes['slider12-value'] : 0,
                'question_13' => isset($attributes['slider13-value']) ? $attributes['slider13-value'] : 0,
                'question_14' => isset($attributes['slider14-value']) ? $attributes['slider14-value'] : 0,
                'question_15' => isset($attributes['slider15-value']) ? $attributes['slider15-value'] : 0,
                'metta' => json_encode([
                    'HTTP_USER_AGENT' => $request->server('HTTP_USER_AGENT'),
                    'REMOTE_ADDR' => $request->server('REMOTE_ADDR'),
                ]),
            ]);
        } catch (\Exception $e) {
           return redirect('/');
        }

        return redirect('/ok');
    }

    /**
     * Редериект на страницу  - что все ок - записалось
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function ok()
    {
        return view('success');
    }
}
