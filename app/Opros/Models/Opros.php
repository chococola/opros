<?php

namespace App\Opros\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Opros
 * @property integer $question_1
 */
class Opros extends Model
{
    /**
     * Подключение из .env
     * @var string
     */
    protected $connection = 'mysql';

    /**
     * Таблица в бд
     * @var string
     */
    protected $table = "opros";

    /**
     * Массив проперти, которые можно заполнять через статический метод модели create
     * @var array
     */
    protected $fillable = [
        'user_first_name',
        'user_second_name',
        'user_class',
        'question_1',
        'question_2',
        'question_3',
        'question_4',
        'question_5',
        'question_6',
        'question_7',
        'question_8',
        'question_9',
        'question_10',
        'question_11',
        'question_12',
        'question_13',
        'question_14',
        'question_15',
        'metta',
    ];
}
