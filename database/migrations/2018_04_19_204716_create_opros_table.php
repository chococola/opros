<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOprosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('opros', function (Blueprint $table) {
            $table->increments('opros_id');
            $table->string('user_first_name')->nullable();
            $table->string('user_second_name')->nullable();
            $table->string('user_class')->nullable();
            $table->integer('question_1')->default(0);
            $table->integer('question_2')->default(0);
            $table->integer('question_3')->default(0);
            $table->integer('question_4')->default(0);
            $table->integer('question_5')->default(0);
            $table->integer('question_6')->default(0);
            $table->integer('question_7')->default(0);
            $table->integer('question_8')->default(0);
            $table->integer('question_9')->default(0);
            $table->integer('question_10')->default(0);
            $table->integer('question_11')->default(0);
            $table->integer('question_12')->default(0);
            $table->integer('question_13')->default(0);
            $table->integer('question_14')->default(0);
            $table->integer('question_15')->default(0);
            $table->longText('metta')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('opros');
    }
}
